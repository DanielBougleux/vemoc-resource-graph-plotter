import matplotlib.pyplot as plotter

def generate_resource_graph(execution_data, extractor_function, line_label, x_label, y_label, isRate=False ,axis=None, color='blue', plotgraph=True , output_path=''):
    
    plotter.close('all')

    first_datapoint = execution_data[0]

    data_timestamp = []
    extracted_data = []
    last_datapoint = None

    for datapoint in execution_data:

        current_extracted_value = 0

        data_timestamp.append((datapoint['timestamp'] - first_datapoint['timestamp']).seconds)

        if datapoint['data']['state'] in ['SUSPENDED', 'STOPPED']:
            current_extracted_value = 0
        else:
            if isRate:
                current_extracted_value = extractor_function(datapoint, last_datapoint)
                last_datapoint = datapoint
            else:
                current_extracted_value = extractor_function(datapoint)
        
        extracted_data.append(current_extracted_value)
    
    if axis == None:

        _, axis = plotter.subplots(ncols=1, nrows=1, constrained_layout=True, figsize=(11,4))
    
    axis.grid(which='major', color='#DDDDDD', linewidth=0.8)
    axis.grid(which='minor', color='#EEEEEE', linestyle=':', linewidth=0.5)

    axis.minorticks_on()

    #what if you want to plot multiple stuff ?
    axis.plot(data_timestamp, extracted_data, label=line_label, color=color)
    axis.set(xlabel=x_label, ylabel=y_label)
    plotter.grid()

    if output_path != '':
        plotter.savefig(output_path)

    if plotgraph:
        plotter.show()
    
    return axis
