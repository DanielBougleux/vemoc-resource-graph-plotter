from database import Database
from Generator import *
import default_functions

db = Database('c512xlarge')

nomes = db.get_all_container_names()

for index, nome in enumerate(nomes):
    print(f'Nome do container de num {index}: {nome}')
    comando = db.get_container_command(nome)
    print(f'Comando do container: {comando}\n')

container_escolhido = int(input('Digite o número do container cujo gráfico deseja gerar: '))
op = int(input('Deseja gerar o gráfico de uso de memória(1), de CPU(2) ou de erro da previsão(3)? '))
nome_da_imagem = input('digite o nome da imagem a ser gerada: ')

dados = db.get_container_execution_data(nomes[container_escolhido])

print(dados[0])


if op == 1:

    generate_resource_graph(dados, default_functions.memory_usage_mib, 'memory usage', 'Time(s)', 'Memory Usage (MiB)')

elif op == 2:

    generate_resource_graph(dados, default_functions.cpu_ms, 'memory usage', 'Time(s)', 'Memory Usage (MiB)', isRate=True)
