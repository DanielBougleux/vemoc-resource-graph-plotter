import jsonpickle
from pymongo import MongoClient

class Database:

    def __init__(self, database_name):
        self.database_connection = None
        self.set_connection(database_name)

    def set_connection(self, database_name):
        try:

            if self.database_connection is not None:
                self.database_connection.close()

            connection = MongoClient()
            self.database_connection = connection[database_name]
            self.database_name = database_name

        except Exception as err:
            print(f'Connection to Database Error: {err}')

    def get_container_command(self, container_name):
        container_collection = self.database_connection[container_name]

        try:
            document = container_collection.find_one()
            data = jsonpickle.decode(document['data'])
            return data['command']
        except Exception as err:
            print(f'Error getting document: {err}')

    def get_container_execution_data(self, container_name):
        
        container_collection = self.database_connection[container_name]
        
        try:

            all_documents = container_collection.find()
            decoded_documents = []
            for document in all_documents:
                decoded_document = {
                    'timestamp': document['timestamp'],
                    'data': jsonpickle.decode(document['data'])
                }
                decoded_documents.append(decoded_document)

            return decoded_documents

        except Exception as err:
            print(f'Error getting container {container_name} history: {err}')


    def get_all_container_names(self):
        
        try:
            return self.database_connection.list_collection_names()
        except Exception as err:
            print(f'Error getting collection names: {err}')