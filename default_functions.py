
#TODO create a more general function where the user can inform the measure unit (GB, MB, etc..)
def memory_usage_mib(datapoint):

    return (int(datapoint['data']['mem_stats']['rss']) + int(datapoint['data']['mem_stats']['cache']))//2 ** 20

def memory_limit_mib(datapoint):

    return (int(datapoint['data']['mem_limit']))//2 ** 20

def swap_mib(datapoint):

    return (int(datapoint['data']['mem_stats']['swap']))//2 ** 20

def cpu_ms(datapoint, last_datapoint):

    current_cpu_usage_ms = int(datapoint['data']['cpu_used'])//1000000
    last_cpu_usage_ms = int(last_datapoint['data']['cpu_used'])//1000000

    return current_cpu_usage_ms - last_cpu_usage_ms