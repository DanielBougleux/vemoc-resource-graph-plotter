from database import Database
from GraphGenerator import *
import os

#configs
results_path1 = '/home/daniel/Área de Trabalho/Estudos/Pesquisa/AWS/Organização de resultados/estudo do erro/resultados200300kvariados/results'
results_path2 = '/home/daniel/Área de Trabalho/Estudos/Pesquisa/AWS/Organização de resultados/estudo do erro/premaderesults/output'
results_path = '/home/daniel/Área de Trabalho/Estudos/Pesquisa/AWS/Organização de resultados/estudo do erro/gradual similarity/gradualsimilarityexperiment/output'
output_path1 = '/home/daniel/Área de Trabalho/Estudos/Pesquisa/AWS/Organização de resultados/estudo do erro/premadeTrabalhados'
output_path = '/home/daniel/Área de Trabalho/Estudos/Pesquisa/AWS/Organização de resultados/estudo do erro/gradual similarity/resswap'
header = 'reference sequence;reference length;target sequence;target length;execution time(s);maximum memory consumption(MiB);maximum prediction error(%);similarity(%);Blocks Pruned(%);MCUPS;matches\n'

db = Database('gradual')

experimentos = os.listdir(results_path)
nomes = db.get_all_container_names()

#scale
ylim = 750
xlim = 200
max_mem = 713

with open(f'{output_path}/resume.csv', 'w') as output_file:
    
    output_file.write(header)

    for experimento in experimentos:

        sequences = experimento.split('-')

        if len(sequences) > 2:
            reference = f'{sequences[0]}-{sequences[1]}'
            target  = f'{sequences[2]}-{sequences[3]}'
        else:
            reference = f'{sequences[0]}'
            target = f'{sequences[1]}'

        print(experimento)

        if f'{experimento}0' in nomes:
            
            dados = db.get_container_execution_data(f'{experimento}0')

            if dados == []:
                continue

            os.mkdir(f'{output_path}/{experimento}')

            try:
                execution_time, maximum_mem_consumption, maximum_error = generate_modeling_error_for_container(f'{output_path}/{experimento}', dados, ylim, xlim, max_mem)
            except:
                print(reference, target)

            hash_path = f'{results_path}/{experimento}/1w'

            try:
                hash_dir = os.listdir(hash_path)[0]
            except:
                continue
            
            with open(f'{hash_path}/{hash_dir}/{target}a.fa/work.tmp/alignment.00.txt') as alignment:

                with open(f'{hash_path}/{hash_dir}/{target}a.fa/work.tmp/statistics_01.00') as statistics:

                    alignment_data = alignment.readlines()
                    statistics_data = statistics.readlines()

                    reference_size = int(alignment_data[0].split('(')[-1].strip(')\n'))
                    target_size = int(alignment_data[1].split('(')[-1].strip(')\n'))

                    matches = int(alignment_data[-4].split(':')[1].strip(' ').split(' ')[0])

                    similarity = matches / max(reference_size, target_size) * 100
                    similarity = round(similarity, 4)

                    mcups = float(statistics_data[21].split(':')[1].strip(' '))
                    blocks_pruned = float(statistics_data[25].split(':')[1].strip(' ').strip('%\n'))

                    experiment_resume = f'{reference};{reference_size};{target};{target_size};{execution_time};{maximum_mem_consumption};{maximum_error};{similarity};{blocks_pruned};{mcups};{matches}\n'

                    output_file.write(experiment_resume)

            

            


